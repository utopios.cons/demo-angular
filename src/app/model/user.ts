export class User {

  firstname: string;
  lastname: string;
  email: string;
  password: string;
  street: string;
  city: string;
  state: string;
  zip: string;
  tel: string;


}
