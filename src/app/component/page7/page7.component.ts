import { Component, OnInit, TestabilityRegistry } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-page7',
  templateUrl: './page7.component.html',
  styleUrls: ['./page7.component.css']
})
export class Page7Component implements OnInit {

  title = 'reactive forms';
  myGroup: FormGroup;
  user: User;
  liste: User[] = [];

  constructor(private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.myGroup = this.formBuilder.group({
      'firstname': this.formBuilder.control('', Validators.required),
      'lastname': this.formBuilder.control('', Validators.required),
      'email': this.formBuilder.control('', [Validators.required, Validators.pattern("^([a-z]*)\@([a-z]{2,10})\.(fr|com)$")]),
      'password': this.formBuilder.control('', [Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")])
      ,
      'street': this.formBuilder.control('', Validators.required),
      'city': this.formBuilder.control('', Validators.required),
      'state': this.formBuilder.control('', Validators.required),
      'zip': this.formBuilder.control('', [Validators.required, Validators.compose([Validators.maxLength(5), Validators.minLength(5)])]),
      'tel': this.formBuilder.control('', [Validators.required, Validators.pattern("^(\+33|0|0033)[1-9][0-9]{8}$")])
    })
  }
  
  get firstname() {
    return this.myGroup.get('firstname'); //notice this
  }
  get password() {
    return this.myGroup.get('password');  //and this too
  }

  get lastname() {
    return this.myGroup.get('lastname'); //notice this
  }
  get email() {
    return this.myGroup.get('email');  //and this too
  }

  get street() {
    return this.myGroup.get('street'); //notice this
  }
  get city() {
    return this.myGroup.get('city');  //and this too
  }
  get state() {
    return this.myGroup.get('state'); //notice this
  }
  get zip() {
    return this.myGroup.get('zip');  //and this too
  }
  get tel() {
    return this.myGroup.get('tel');  //and this too
  }


  handleSubmit() {

    if (this.myGroup.valid) {

      this.user = new User();

      /* this.user.firstname = this.myGroup.get('firstname').value;
      this.user.lastname = this.myGroup.get('lastname').value;
      this.user.email = this.myGroup.get('email').value;
      this.user.password = this.myGroup.get('password').value;
      this.user.city = this.myGroup.get('city').value;
      this.user.state = this.myGroup.get('state').value;
      this.user.street = this.myGroup.get('street').value;
      this.user.zip = this.myGroup.get('zip').value;
      this.user.tel = this.myGroup.get('tel').value; */

      this.user = this.myGroup.value;


      if (localStorage.getItem('liste') == null) {
        this.liste.push(this.user);
        localStorage.setItem('liste', JSON.stringify(this.liste));
      } else {
        this.liste = JSON.parse(localStorage.getItem('liste'));
        this.liste.push(this.user);
        console.log(this.liste);
        localStorage.setItem('liste', JSON.stringify(this.liste));


      }


    }



    //return this.myGroup.valid ? this.router.navigate(["/affichage-form", this.user.id]) : this.router.navigate(["/page1"])
  }



}
