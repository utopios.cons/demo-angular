import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, filter, flatMap, Observable, throwError, toArray } from 'rxjs';
import { Post } from '../model/post';
import { Todo } from '../model/todo';

@Injectable({
  providedIn: 'root'
})
export class DataTodoService {

  url2: string = "http://localhost:3000/posts";

  todo: Todo;

  constructor(private http: HttpClient) { }

 /*  getTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.url);
  }



  getTodo(id: string): Observable<Todo> {
    return this.http.get<Todo>(this.url + "/" + id);
  }


  getTodoCompleted(complete: boolean): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.url).pipe(
      flatMap(e => e),
      filter((x: Todo) => x.completed === true),
      toArray()
    )
  }


  createTodo(todo: Todo): Observable<any> {
    const httpOptions: { headers: any; observe: any; } = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response'
    };
    return this.http.post(this.url, todo, httpOptions)
  }


  createTodoWithTodoReturn(todo: Todo): Observable<Todo> {
    return this.http.post<Todo>(this.url, todo);
  }


  updateTodo(todo: Todo): Observable<Todo> {
    return this.http.put<Todo>(this.url, todo)
  }


  deleteTodo(id: any): Observable<any> {
    const httpOptions: { headers: any; observe: any; } = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response'
    };
    return this.http.delete(this.url + "/" + id, httpOptions);
  }; */

  // Post :

  getPosts(): Observable<any[]> {
    return this.http.get<any[]>(this.url2);
  }

  getPostById(id: any): Observable<any> {
    return this.http.get<Post>(this.url2 + "/" + 78, {responseType:'json'})
    .pipe(
      catchError((err) => {
        console.log('une erreur est arrivée')
        console.error( "l'erreur est : "+ err.status);
        return throwError(err);
      }))
  }

  deletePost(id: any): Observable<any> {
    const httpOptions: { headers: any; observe: any; } = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response'
    };
    return this.http.delete(this.url2 + "/" + id, httpOptions);
  }

  createPost(post: Post): Observable<any> {
    const httpOptions: { headers: any; observe: any; } = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response'
    };
    return this.http.post(this.url2, post, httpOptions);

  }

  update(post: Post): Observable<any> {
    console.log(post)
    const httpOptions: { headers: any; observe: any; responseType: any } = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response',
      responseType: 'text'
    };
    return this.http.put(this.url2 + "/" + post.id, post, httpOptions);
  }


  getPostWithParams(para1: any, para2: any, para3: any): Observable<any> {

    const params = new HttpParams()
      .set('para1', para1)
      .set('para2', para2);
    params.append('para3', para3);


    //let params = new HttpParams({ fromObject: { page: PageNo, sort: SortOn } }); => object

    return this.http.get<any[]>(this.url2, { params });

  }


  postImage(file: File): Observable<any> {
    let formData: FormData = new FormData();
    formData.append('uploadFile', file, file.name);
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let requestOption = { headers: headers, reportProgress: true }
    return this.http.post<any>(this.url2, formData, requestOption);
  }
}




