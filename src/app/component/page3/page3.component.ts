import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { HeaderService } from 'src/app/service/header.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-page3',
  templateUrl: './page3.component.html',
  styleUrls: ['./page3.component.css']
})
export class Page3Component implements OnInit {


  message: string = "Salut l'ami";
  nombre: number = 0;
  nombre2: any = 0;
  titre: string = "Les pipes"
  date: Date = new Date();

  monPrenom: string = "Mohamed";
  formation: string = "Hello les amis"
  use: {} = " "
  tab: any[] = [];
  donne: number = Math.floor(Math.random() * 10) / 1000
  donne2: number = Math.floor(Math.random() * 100)

  tabString: string[] = ["Marco", "Michel", "Paul", "Jack", "Mike", "John", "Tom", "Sam", "Mathieu"];

  constructor(private dataService: DataService, private headerService: HeaderService,
    public dialog: MatDialog, private route: ActivatedRoute) {

    this.nombre = this.route.snapshot.params['id'];
    this.route.params.subscribe((param: Params) => { this.nombre2 = param['id'] });
    console.log("nombre3 " + this.nombre2);

  }

  openDialog(): void {
    console.log("openDialog");
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '250px',
      data: { message: this.message, nombre: this.nombre }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.message = result;
    });
  }




  ngOnInit(): void {
    this.openDialog();
    this.headerService.changeTitre(this.titre);
    this.tab = this.dataService.getData();
    this.use = this.tab[0];

  }




}
function param(param: any, Params: any) {
  throw new Error('Function not implemented.');
}

