import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-page5',
  templateUrl: './page5.component.html',
  styleUrls: ['./page5.component.css']
})
export class Page5Component implements OnInit, OnDestroy {

  message: string = "Je suis une phrase";
  titre: string = "Le cycle de vie des composants"

  constructor(private headerService: HeaderService) { }

  ngOnInit(): void {

    this.headerService.changeTitre(this.titre);

    console.log("OnInit");
    setTimeout(() => {
      alert("Changement")
      this.message = "J'ai changé !!"
    }, 5000);
  }


  ngOnChanges() {
    alert("onChange");
  }


  ngAfterContentInit() {

    alert("AfterContentInit");

  }
  ngAfterContentChecked() {
    alert("AfteContentCheck");

  }
  ngAfterContentChanged() {
    alert("AfterContentChanged");

  }
  ngAfterViewInit() {
    console.log("AfterViewInit");
  }
  ngAfterViewChecked() {
    alert("AfterViewChecked");

  }
  ngAfterViewChanged() {
    alert("AfterViewChange");

  }


  ngOnDestroy() {
    alert("onDestroy");
  }


}
