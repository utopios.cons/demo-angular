import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-page6',
  templateUrl: './page6.component.html',
  styleUrls: ['./page6.component.css']
})
export class Page6Component implements OnInit {

  titre: string = "Le routing";

  constructor(private router: Router, private headerService: HeaderService) { }

  ngOnInit(): void {
    this.headerService.changeTitre(this.titre);
  }

  navigation(valeur: any) {

    this.router.navigate(['/page6', valeur.target.value]);
    //alert(valeur.target.value);

  }

}
