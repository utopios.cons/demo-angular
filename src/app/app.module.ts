import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Page1Component } from './component/page1/page1.component';
import { Page2Component } from './component/page2/page2.component';
import { HeaderComponent } from './shared/header/header.component';
import { Page3Component } from './component/page3/page3.component';
import { NavsideComponent } from './shared/navside/navside.component';
import { AcroPipe } from './tools/pipe/acro.pipe';
import { StartMPipe } from './tools/pipe/start-m.pipe';
import { Page4Component } from './component/page4/page4.component';
import { CardComponent } from './shared/card/card.component';
import { ErrorComponent } from './component/error/error.component';
import { BackRedDirective } from './tools/directive/back-red.directive';
import { Page5Component } from './component/page5/page5.component';
import { Page6Component } from './component/page6/page6.component';
import { ModalComponent } from './shared/modal/modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { Page7Component } from './component/page7/page7.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AffichageFormComponent } from './component/affichage-form/affichage-form.component';
import { Page8Component } from './component/page8/page8.component';
import { HttpClientModule } from '@angular/common/http';








@NgModule({
  declarations: [
    AppComponent,
    Page1Component,
    Page2Component,
    HeaderComponent,
    Page3Component,
    NavsideComponent,
    AcroPipe,
    StartMPipe,
    Page4Component,
    CardComponent,
    ErrorComponent,
    BackRedDirective,
    Page5Component,
    Page6Component,
    ModalComponent,
    Page7Component,
    AffichageFormComponent,
    Page8Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
