import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit, OnDestroy {


  @Input()
  inputFromParent: string | undefined;

  @Output()
  outputFromChild: EventEmitter<string> = new EventEmitter();
  outputText: string = "Adopte moi !!!";


  sendDataToParent() {
    this.outputFromChild.emit(this.outputText);
  }

  constructor() { }

  ngOnInit(): void {

  }


  ngOnDestroy(){
    alert("destroying child...");
  }


}
