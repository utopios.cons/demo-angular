import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Post } from 'src/app/model/post';
import { Todo } from 'src/app/model/todo';
import { DataTodoService } from 'src/app/service/data-todo.service';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-page8',
  templateUrl: './page8.component.html',
  styleUrls: ['./page8.component.css']
})
export class Page8Component implements OnInit {

  title: string = "Les posts"
  liste: Todo[] = [];
  listeComplete: Todo[] = [];
  todo: Todo;
  id: string = "1"
  id2: any = "1";


  form: FormGroup;
  post: Post;
  posts: Post[] = [];



  constructor(private dataTodoService: DataTodoService, private fb: FormBuilder, private headerService: HeaderService) { }

  ngOnInit(): void {
    this.headerService.changeTitre(this.title);
    this.form = this.fb.group({
      id: ['', Validators.required],
      titre: [' ', Validators.required],
      description: [' ', Validators.required],
      date: [' ', Validators.required]
    })
    this.getPost();
  }




  get titre() {
    return this.form.get('titre');
  }

  get description() {
    return this.form.get('description');
  }

  get date() {
    return this.form.get('date');
  }

  validation() {
    if (this.form.valid) {
      this.post = this.form.value;
      console.log(this.post.id);
      if (this.post.id == undefined) {
        this.dataTodoService.createPost(this.post).subscribe(response => {
          alert(response.status);
          this.getPost();
        })
      } else {
        this.dataTodoService.update(this.post).subscribe(response => {
          alert(response.status);
          this.getPost();
        })
      }
    }
  }


  getPost() {
    this.dataTodoService.getPosts().subscribe((posts) => {
      this.posts = posts
    })
  }

  edit(id: number) {
    console.log(this.titre);
    this.dataTodoService.getPostById(id).subscribe(post => {
      console.log(post)
      this.form.get('id').patchValue(post.id);
      this.form.get('titre').patchValue(post.titre);
      this.form.get('description').patchValue(post.description);
      this.form.get('date').patchValue(post.date);
    })
  }



  delete(id: any) {
    this.dataTodoService.deletePost(id).subscribe((response) => {
      if (response.status == "204" || "200") {
        alert("Reponse 200: " + response.status);
        this.getPost();
      } else {
        alert("Reponse : pas 200" + response.status);
        this.getPost();
      }
    })
  }

}
