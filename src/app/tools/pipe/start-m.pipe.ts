import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'startM'
})
export class StartMPipe implements PipeTransform {

  tab: string[] = [];


  transform(args: string[]): string[] {
    args.forEach(x => {

      if (x.startsWith('M')) {
        this.tab.push(x);
      }

    });
    return this.tab;

  }

}
