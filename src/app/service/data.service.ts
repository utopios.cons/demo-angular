import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {


  data: any[] = [{
    "name": "Keefe Wise",
    "phone": "1-285-253-1653",
    "email": "sed.eu@protonmail.org",
    "address": "Ap #922-1532 A Rd.",
    "postalZip": "584481",
    "region": "Huádōng",
    "country": "Philippines"
  },
  {
    "name": "Remedios Alvarado",
    "phone": "1-806-401-2255",
    "email": "donec.tempor@icloud.edu",
    "address": "7054 Porttitor Street",
    "postalZip": "7762",
    "region": "Warmińsko-mazurskie",
    "country": "Spain"
  },
  {
    "name": "Audrey Butler",
    "phone": "1-263-956-1621",
    "email": "lorem.ipsum@google.org",
    "address": "115-2002 Vulputate Rd.",
    "postalZip": "2188",
    "region": "Östergötlands län",
    "country": "Ireland"
  },
  {
    "name": "Ian Willis",
    "phone": "(742) 127-2506",
    "email": "commodo.auctor.velit@hotmail.ca",
    "address": "P.O. Box 715, 4511 At, Ave",
    "postalZip": "143382",
    "region": "Dalarnas län",
    "country": "South Korea"
  },
  {
    "name": "Gillian Martinez",
    "phone": "(218) 569-0800",
    "email": "ultricies.adipiscing@outlook.ca",
    "address": "Ap #246-3157 Lectus Av.",
    "postalZip": "585658",
    "region": "Northern Cape",
    "country": "Spain"
  },
  {
    "name": "Holmes Barton",
    "phone": "(455) 746-2612",
    "email": "elit.etiam@outlook.com",
    "address": "Ap #781-9525 Ipsum. Ave",
    "postalZip": "872495",
    "region": "Madrid",
    "country": "Singapore"
  },
  {
    "name": "Ella Richmond",
    "phone": "1-386-316-0694",
    "email": "nunc.id@yahoo.ca",
    "address": "3848 In St.",
    "postalZip": "25-94",
    "region": "Noord Holland",
    "country": "Belgium"
  },
  {
    "name": "Alisa Decker",
    "phone": "(347) 213-3232",
    "email": "quisque.ac@icloud.com",
    "address": "834-1156 Adipiscing Avenue",
    "postalZip": "23-94",
    "region": "North-East Region",
    "country": "South Korea"
  },
  {
    "name": "Stella Spencer",
    "phone": "(441) 703-7383",
    "email": "et@yahoo.edu",
    "address": "347-4547 A, Road",
    "postalZip": "5547",
    "region": "Kinross-shire",
    "country": "Turkey"
  },
  {
    "name": "Ray Boone",
    "phone": "(664) 808-1235",
    "email": "dictum.ultricies@yahoo.org",
    "address": "481-812 Luctus Road",
    "postalZip": "78118",
    "region": "Himachal Pradesh",
    "country": "Vietnam"
  }
  ]


  getData() {
    return this.data;

  }

  constructor() { }
}
