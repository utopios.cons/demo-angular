import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.css']
})
export class Page2Component implements OnInit {

  titre: string = "Les directives";
  isVisible: boolean = false;
  compteur: number = 0;
  isValid: boolean = true;
  isLoggedIn: boolean = true;
  indicateur: number = 0;

  changement() {
    this.isVisible = !this.isVisible;
    this.isValid = !this.isValid;
    this.isLoggedIn = !this.isLoggedIn;
    this.compteur++;
  }


  person: string = " ";

  tab: string[] = ["Marco", "Michel", "Alain", "Pierre", "Tom"];

  changeName() {
    this.indicateur++;
    if (this.indicateur <= 4) {
      this.person = this.tab[this.indicateur];
    } else {
      this.indicateur = 0;
      this.person = this.tab[this.indicateur];
    }
  }

  user: any[] = [];

  constructor(private dataService: DataService, private headerService: HeaderService) {
    this.user = this.dataService.getData();
  }

  valeur: boolean = true;

  change() {
    this.valeur = !this.valeur;

  }

  alert: string = "alert alert-warning";

  ngOnInit(): void {
    setTimeout(() => {
      this.alert = "alert alert-danger"
    }, 3000);

    this.headerService.changeTitre(this.titre);

  }

}


