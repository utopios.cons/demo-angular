import { Component, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/service/header.service';


@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit {

  titre: string = "Le binding";

  // interpolation
  myName: string = "Mohamed";
  myAge: number = 42;
  isBeauGoss = false;

  // property binding
  person: string = 'Marco Polo';

  age: number = 30;

  address: any = { street: 'rue du Paradis', city: '75010 Paris' };

  alignement: string = 'right';

  mot: string = "urgent";
  alert: string = "alert alert-danger";
  isDanger: boolean = true;

  changeMot() {
    this.isDanger = !this.isDanger;
    this.alert = this.isDanger ? "alert alert-danger" : "alert alert-success";
    this.mot = this.isDanger ? "Urgent" : "Pas urgent"
  }


  // event binding

  modification: string = " "
  modifie(value: any) {
    console.log(value.target.value);
    this.modification = value.target.value;

  }

  modifieEncore2: number = 0;

  modifieEncore(valeur: any) {
    console.log(valeur)
    console.log(valeur.target.value);
    this.modifieEncore2 = valeur.target.value;
  }


  focused() {
    console.log("focus");
  }


  // Two way binding
  username: string = 'Michou';


  constructor(private headerService: HeaderService) { }

  ngOnInit(): void {

    this.headerService.changeTitre(this.titre);

  }

}
