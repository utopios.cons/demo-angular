import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AffichageFormComponent } from './component/affichage-form/affichage-form.component';
import { ErrorComponent } from './component/error/error.component';
import { Page1Component } from './component/page1/page1.component';
import { Page2Component } from './component/page2/page2.component';
import { Page3Component } from './component/page3/page3.component';
import { Page4Component } from './component/page4/page4.component';
import { Page5Component } from './component/page5/page5.component';
import { Page6Component } from './component/page6/page6.component';
import { Page7Component } from './component/page7/page7.component';
import { Page8Component } from './component/page8/page8.component';


const routes: Routes = [

  { path: 'page1', component: Page1Component },
  { path: 'page2', component: Page2Component },
  // { path: 'page3/:id', component: Page3Component },
  { path: 'page4', component: Page4Component },
  //  { path: 'page5/:id/other', component: Page5Component },
  // { path: 'page6', component: Page6Component },
  {
    path: 'page6', children: [
      { path: 'home', component: Page6Component },
      { path: ':id', component: Page3Component },
      { path: ':id/other', component: Page5Component }
    ]
  },
  { path: 'page7', component: Page7Component },
  { path: 'affichage-form/:userId', component: AffichageFormComponent },
  { path: 'page8', component: Page8Component },
  { path: '', component: Page1Component },
  { path: '**', component: ErrorComponent, data: { message: "Page non trouve" } },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
