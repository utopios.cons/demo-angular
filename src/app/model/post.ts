export interface Post {
  id?:number;
  titre: string;
  description: string;
  date: Date;
}
