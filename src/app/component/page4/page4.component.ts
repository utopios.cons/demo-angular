import { Component, HostListener, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-page4',
  templateUrl: './page4.component.html',
  styleUrls: ['./page4.component.css']
})
export class Page4Component implements OnInit {

  titre: string = "Les composants"
  inputText: string = 'Milou';
  message: string = "";

  receiveChildData(data: any) {
    this.message = "Adopte moi !!"
  }


  constructor(private headerService: HeaderService) { }

  ngOnInit(): void {

    /* console.log("OnInit");
    setTimeout(() => {
      alert("Changement")
      this.message = "Merci"
    }, 5000); */
    this.headerService.changeTitre(this.titre);

  }


  /* ngOnChanges() {
    alert("onChange");
  }


  ngAfterContentInit() {

    alert("AfterContentInit");

  }
  ngAfterContentChecked() {
    alert("AfteContentCheck");

  }
  ngAfterContentChanged() {
    alert("AfterContentChanged");

  }
  ngAfterViewInit() {
    alert("AfterViewInit");
  }
  ngAfterViewChecked() {
    alert("AfterViewChecked");

  }
  ngAfterViewChanged() {
    alert("AfterViewChange");

  }

  @HostListener('window:beforeunload')
  ngOnDestroy() {
    alert("onDestroy");
  } */


}
