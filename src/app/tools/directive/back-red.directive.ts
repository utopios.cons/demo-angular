import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appBackRed]'
})
export class BackRedDirective implements OnInit {

  constructor(private _elementRef: ElementRef) {
  }

  ngOnInit() {
    this._elementRef.nativeElement.style.backgroundColor = 'red';
  }

}
