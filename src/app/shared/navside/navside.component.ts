import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navside',
  templateUrl: './navside.component.html',
  styleUrls: ['./navside.component.css']
})
export class NavsideComponent implements OnInit {

  id: number = 13;

  constructor(private router: Router) { }


  navigate() {
    this.router.navigate(['/page4']);
  }


  ngOnInit(): void {
  }

}
