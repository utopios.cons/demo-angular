import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'acroNyme',
  pure: true
})
export class AcroPipe implements PipeTransform {

  val: any;
  transform(value: string): string {
    this.val = value.split(" ").map(x => x[0]).join(".").toUpperCase();
    console.log(this.val);
    return this.val;
  }

}
